from datetime import datetime, timedelta

from shared.reports.html_default_config import *
from shared.reports.report import fetch_report, generate_csv_file, send_mail
from shared.reports.connection_params import *
from shared.utilities.zip_file import zip_file

from main_logger import logger_corporative_client_daily_report


try:
    logger_corporative_client_daily_report.info('Corporative Client Daily Report generation started.')
    data_records = fetch_report('DAILY', 'CORPORATIVE_CLIENT')
    logger_corporative_client_daily_report.info('Report list to be generated are fetched.')
    for data in data_records:
        try:
            report_id = data[0]
            report_name = data[1]
            schedule = data[2]
            sql_query = data[3]
            is_html = data[4]
            db_connection_name = data[5]
            schedule_type = data[6]
            mail_to = str(data[7]).split(",")
            mail_cc = str(data[8]).split(",")
            mail_bcc = str(data[9]).split(",")
            mail_subject = data[10]
            mail_body = data[11]
            html_config_id = data[12]
            html_header = data[13]
            html_footer = data[14]
            html_signature = data[15]
            create_zip_file = data[16]
            encryption_value = data[17]

            # Obtaining yesterday's date for report name concatenation
            today_date = datetime.now()
            today_date_str = '_' + today_date.strftime('%Y-%m-%d_%H%M%S')
            today = today_date.strftime('%Y-%m-%d')
            mail_body_args = {'date': today}

            csv_file_path = generate_csv_file(db_connection(db_connection_name),
                                              sql_query,
                                              report_name,
                                              today_date_str)

            if is_html:
                # compose html file
                mail_body = html_composer(csv_file_path,
                                          html_config_id,
                                          html_header,
                                          html_footer,
                                          html_signature
                                          )

            if csv_file_path:
                destination_zip_file_path = csv_file_path.split('.')[0] + '.zip'

                if create_zip_file:
                    append_date = today_date.strftime('%Y%m%d')
                    zip_file(report_name, csv_file_path, destination_zip_file_path, encryption_value, append_date)

                # Send Mail
                file_path = csv_file_path if not create_zip_file else destination_zip_file_path
                send_mail_status = send_mail(mail_to,
                                             mail_cc,
                                             mail_bcc,
                                             mail_subject,
                                             mail_body,
                                             mail_body_args,
                                             file_path,
                                             is_html,
                                             schedule_type)
                if send_mail_status == 1:
                    logger_corporative_client_daily_report.info(f'Mail successfully sent for {report_name}.')
                else:
                    logger_corporative_client_daily_report.warning(f'Error while sending mail for report: {report_name}')
            else:
                logger_corporative_client_daily_report.info(f'No records generated for report: {report_name}')

        except Exception as e:
            logger_corporative_client_daily_report.error(e, exc_info=True)

    logger_corporative_client_daily_report.info('Corporative Client Daily Report generation completed.')

except Exception as e:
    logger_corporative_client_daily_report.error(e, exc_info=True)

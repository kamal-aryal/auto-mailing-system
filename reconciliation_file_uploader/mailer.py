import configparser

import mailerpy as mailer
from logger_reconciliation import logger_mailer
import html_config


config = configparser.RawConfigParser()
config_path = 'D:/eSewa Project/esewa_services/config_file.properties'
config.read(config_path)

# Mail Config
SMTP_HOST = config.get('MAIL_CONFIG', 'SMTP_HOST')
SMTP_PORT = config.get('MAIL_CONFIG', 'SMTP_PORT')
MAIL_FROM = config.get('MAIL_CONFIG', 'MAIL_FROM')
MAIL_PASSWORD = config.get('MAIL_CONFIG', 'MAIL_PASSWORD')


my_mailer = mailer.Mailer(SMTP_HOST, SMTP_PORT, MAIL_FROM, MAIL_PASSWORD)
mail_receivers = ['kmlaryal2@gmail.com']
mail_cc = ['']
mail_subject = 'Reconciliation Job File Upload'
# mail_bcc = []


def send_mail(file_name, load_status):
    if load_status == 'SUCCESS':
        body_message = 'File {file_name} is loaded successfully.'.format(file_name=file_name)
        html_body = html_config.html_composer(body_message)
    elif load_status == 'FAILED':
        body_message = 'Loading of file {file_name} failed. Please contact DBA for further information.'.format(file_name=file_name)
        html_body = html_config.html_composer(body_message)

    try:

        my_mailer.send_mail(to_address=mail_receivers,
                            subject=mail_subject,
                            mail_body=html_body,
                            mail_cc=mail_cc,
                            file_as_html=True)
    except Exception as e:
        logger_mailer.error(e, exc_info=True)


import os
import logging


# Path for saving Log Files
reconciliation_dir_path = os.path.abspath(os.path.dirname(__file__))
log_path = os.path.join(reconciliation_dir_path, "./log_files/")

if not os.path.exists(os.path.dirname(log_path)):
    try:
        os.mkdir(log_path)
    except OSError as e:
        print(f'Directory creation failed: {log_path}')


# Initializing Logger for db_helper modules
logger_reconciliation = logging.getLogger('reconciliation')
logger_mailer = logging.getLogger('mailer')

# Setting Logging Level
logger_reconciliation.setLevel(logging.DEBUG)
logger_mailer.setLevel(logging.DEBUG)

# Defining Logging Format
formatter_reconciliation = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_mailer = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')

# Path for logging file handlers
file_handler_reconciliation = logging.FileHandler(log_path + 'reconciliation.log')
file_handler_mailer = logging.FileHandler(log_path + 'mailer.log')

# Setting Logging Level for File Handler
file_handler_reconciliation.setLevel(logging.DEBUG)
file_handler_mailer.setLevel(logging.DEBUG)

# Setting Formatter to file handlers
file_handler_reconciliation.setFormatter(formatter_reconciliation)
file_handler_mailer.setFormatter(formatter_mailer)

# Adding file handler to logger
logger_reconciliation.addHandler(file_handler_reconciliation)
logger_mailer.addHandler(file_handler_mailer)

# Adding Stream Handler
stream_handler_reconciliation = logging.StreamHandler()
stream_handler_mailer = logging.StreamHandler()

stream_handler_reconciliation.setFormatter(formatter_reconciliation)
stream_handler_mailer.setFormatter(formatter_mailer)

logger_reconciliation.addHandler(stream_handler_reconciliation)
logger_mailer.addHandler(stream_handler_mailer)

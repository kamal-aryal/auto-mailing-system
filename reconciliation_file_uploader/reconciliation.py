import os
import time
import contextlib
import configparser

from sqlalchemy import create_engine
import pandas as pd

from mailer import send_mail
from logger_reconciliation import logger_reconciliation

config = configparser.RawConfigParser()
config_path = 'D:/eSewa Project/esewa_services/config_file.properties'
# config_path = '/opt/ams/esewa_services/config_file.properties'
config.read(config_path)

working_dir = config.get('WORKING_DIRECTORIES', 'RECONCILIATION_MAIN_PATH')
upload_place = config.get('WORKING_DIRECTORIES', 'RECONCILIATION_UPLOAD_PATH')
connection_credential = config.get('DB_CONNECTION_PARAMETER', 'CONNECT_DB_RECONCILIATION')

# Creating Default Directories
vendor_list = ['NTC', 'NCELL', 'CTP']
stages_list = ['PROCESS_PLACE', 'SUCCESS_PLACE', 'FAILED_PLACE']

for each_stage in stages_list:
    for each_vendor in vendor_list:
        folder_name = f'{working_dir}/{each_stage}/{each_vendor}/'

        if not os.path.exists(os.path.dirname(folder_name)):
            try:
                os.makedirs(folder_name)
            except OSError:
                logger_reconciliation.error(f"Creation of {folder_name} directory failed.")


@contextlib.contextmanager
def connect(connection_string):
    """
    Creates a connection to database, returns connection to calling program and finally closes connection

    Parameters
    ----------
    connection_string : STRING
        String that is used for making connection to database

    Returns
    -------
    connection object
    """
    try:
        engine = create_engine(connection_string, encoding='utf8', echo=False)
        connection = engine.connect()
    except Exception as e:
        logger_reconciliation.error(e, exc_info=True)

    try:
        yield connection
    finally:
        engine.dispose()


def process_and_insert_ncell_file(file_data):

    file_data.rename(columns={
                            'Vendor Trace Id': 'VENDOR_TRACE_ID',
                            'Mobile Number': 'SERVICE_ATTRIBUTE',
                            'Vendor Trans Id': 'VENDOR_TRANS_ID',
                            'Amount': 'AMOUNT',
                            'Vendor Code': 'VENDOR_CODE',
                            'Vendor Description': 'VENDOR_DESCRIPTION',
                            'Transaction Date': 'TXN_DATE'
                            },
                     inplace='TRUE')
    file_data['TXN_DATE'] = pd.to_datetime(file_data['TXN_DATE'], dayfirst=True)

    with connect(connection_credential) as connection:
        file_data[['VENDOR_TRACE_ID', 'SERVICE_ATTRIBUTE', 'VENDOR_TRANS_ID', 'AMOUNT', 'VENDOR_CODE',
                   'VENDOR_DESCRIPTION', 'TXN_DATE', 'FILE_ID']].to_sql('NCELL_TOPUP_RECORD',
                                                                        con=connection,
                                                                        if_exists='append',
                                                                        index=False,
                                                                        chunksize=10000)
        logger_reconciliation.info(f'NCELL File data inserted for FILE_ID: {file_data.FILE_ID.head(1)}')


def process_and_insert_ntc_file(file_data):
    file_data.rename(columns={'TransactionId': 'VENDOR_TRANS_ID', 'PhoneId': 'SERVICE_ATTRIBUTE',
                              'Amount': 'AMOUNT', 'StartDate': 'TXN_DATE'}, inplace='TRUE')
    file_data['TXN_DATE'] = pd.to_datetime(file_data['TXN_DATE'])

    with connect(connection_credential) as connection:
        file_data[['FILE_ID', 'VENDOR_TRANS_ID', 'SERVICE_ATTRIBUTE', 'AMOUNT',
                   'TXN_DATE']].to_sql('NTC_TOPUP_RECORD', con=connection, if_exists='append', index=False, chunksize=10000)
        logger_reconciliation.info(f'NTC File data inserted for FILE_ID: {file_data.FILE_ID.head(1)}')


class ReconFile:
    def __init__(self, file_name):
        self.file_name = file_name

        if file_name.startswith('NTC'):
            self.vendor = 'NTC'

        elif file_name.startswith('NCELL'):
            self.vendor = 'NCELL'

        elif file_name.startswith('CTP'):
            self.vendor = 'CTP'

    def is_loaded(self):
        return self.file_name in os.listdir(working_dir + '/SUCCESS_PLACE/' + self.vendor)

    def is_loaded_in_db(self):
        return self.get_file_id() is not None

    def move_to_loaded(self):
        os.rename(working_dir + '/PROCESS_PLACE/' + self.vendor + '/' + self.file_name,
                  working_dir + '/SUCCESS_PLACE/' + self.vendor + '/' + self.file_name)

    def move_to_failed(self):
        os.rename(working_dir + '/PROCESS_PLACE/' + self.vendor + '/' + self.file_name,
                  working_dir + '/FAILED_PLACE/' + self.vendor + '/' + self.file_name + str(time.time()))

    def move_to_processing(self):
        os.rename(upload_place + self.file_name,
                  working_dir + '/PROCESS_PLACE/' + self.vendor + '/' + self.file_name)

    def load_to_recon_config(self):
        with connect(connection_credential) as connection_object:
            connection_object.execute("INSERT INTO RECONCILIATION_CONFIG(FILENAME, STATUS, TYPE) values"
                                      "('{}','PROCESSING','{}')".format(self.file_name, self.vendor))
    def load_to_recon_config(self):
        with connect(connection_credential) as connection_object:
            connection_object.execute("INSERT INTO RECONCILIATION_CONFIG(FILENAME, STATUS, TYPE) values"
                                      "('{}','PROCESSING','{}')".format(self.file_name, self.vendor))

    def get_file_id(self):
        with connect(connection_credential) as connection_object:
            db_load_result = connection_object.execute(
                'SELECT ID from RECONCILIATION_CONFIG where filename="{}" and status IN ("SUCCESS","PROCESSING");'.format(self.file_name)).fetchone()
            return db_load_result[0] if db_load_result is not None else None

    def load_to_recon_table(self):
        file_data = pd.read_csv(working_dir + '/PROCESS_PLACE/' + self.vendor + '/' + self.file_name,
                                engine='python', index_col=None, dtype=str)
        file_data['FILE_ID'] = self.get_file_id()

        try:
            if self.vendor == 'NTC':
                logger_reconciliation.info(f'Calling loader function for NTC.')
                process_and_insert_ntc_file(file_data)
            elif self.vendor == 'NCELL':
                logger_reconciliation.info(f'Calling loader function for NCELL.')
                process_and_insert_ncell_file(file_data)

            # change status to success
            logger_reconciliation.info(f'Status update to SUCCESS for file: {self.file_name}')
            self.update_status_in_recon_config('SUCCESS')

            # move to success place
            logger_reconciliation.info(f'Moving file to SUCCESS_PLACE for file: {self.file_name}')
            self.move_to_loaded()

            # sent success mail
            logger_reconciliation.info(f'Sending success mail for file: {self.file_name}')
            send_mail(self.file_name, load_status='SUCCESS')

        except Exception as e:
            logger_reconciliation.error(e, exc_info=True)
            # change status to fail
            logger_reconciliation.info(f'Status updated to FAILED for file: {self.file_name}')
            self.update_status_in_recon_config('FAILED')

            # move to failed place
            logger_reconciliation.info(f'Moving file to FAILED_PLACE for file: {self.file_name}')
            self.move_to_failed()

            # send failure mail
            logger_reconciliation.info(f'Sending failure mail for file: {self.file_name}')
            send_mail(self.file_name, load_status='FAILED')

    def update_status_in_recon_config(self, status):
        with connect(connection_credential) as db_connection:
            db_connection.execute("UPDATE RECONCILIATION_CONFIG SET STATUS = '{}' where "
                                  "FILENAME ='{}' and STATUS ='PROCESSING';".format(status, self.file_name))


file_list = os.listdir(upload_place)

for file in file_list:
    try:
        logger_reconciliation.info(f'Reconciliation file uploader started for file: {file}')
        file_object = ReconFile(file)

        # It already present then it will throw error due to same name and stay in processing
        # It will be processed again after 5 minutes and this time it executes
        logger_reconciliation.info(f'Moving file to PROCESS_PLACE: {file}')
        file_object.move_to_processing()

        if file_object.is_loaded() or file_object.is_loaded_in_db():
            logger_reconciliation.info(f'File already loaded: {file}')

            logger_reconciliation.info(f'Moving file to FAILED_PLACE: {file}')
            file_object.move_to_failed()

            logger_reconciliation.info(f'Sending failed mail for file: {file}')
            send_mail(file_object.file_name, load_status='FAILED')
            logger_reconciliation.info(f'Failed mail sent for file: {file}')
            continue

        logger_reconciliation.info(f'Inserting to RECONCILIATION_CONFIG table for file: {file}')
        file_object.load_to_recon_config()

        logger_reconciliation.info(f'Loading data from file: {file}')
        file_object.load_to_recon_table()

        logger_reconciliation.info(f'Reconciliation file uploader successfully completed for file: {file}')
    except Exception as e:
        logger_reconciliation.error(e, exc_info=True)

import os
import logging


# Path for saving Log Files
db_helper_dir_path = os.path.abspath(os.path.dirname(__file__))
db_log_path = os.path.join(db_helper_dir_path, "./log_files/")

if not os.path.exists(os.path.dirname(db_log_path)):
    try:
        os.mkdir(db_log_path)
    except OSError as e:
        print(f'Directory creation failed: {db_log_path}')


# Initializing Logger for db_helper modules
logger_generate_daily_report = logging.getLogger('generate_daily_report')
logger_ibft_summary_report = logging.getLogger('ibft_summary_report')
logger_manual_ibft_success = logging.getLogger('manual_ibft_success')
logger_merchant_txn_list_after_job = logging.getLogger('merchant_txn_list_after_job')
logger_every_sunday = logging.getLogger('every_sunday')
logger_generate_monthly_report = logging.getLogger('generate_monthly_report')
logger_bank_topup_status_cs = logging.getLogger('bank_topup_status_cs')
logger_screen_report = logging.getLogger('screen_report')
logger_daily_at_ten = logging.getLogger('daily_at_ten')
logger_corporative_client_daily_report = logging.getLogger('corporative_client_daily_report')

# Setting Logging Level
logger_generate_daily_report.setLevel(logging.DEBUG)
logger_ibft_summary_report.setLevel(logging.DEBUG)
logger_manual_ibft_success.setLevel(logging.DEBUG)
logger_merchant_txn_list_after_job.setLevel(logging.DEBUG)
logger_every_sunday.setLevel(logging.DEBUG)
logger_generate_monthly_report.setLevel(logging.DEBUG)
logger_bank_topup_status_cs.setLevel(logging.DEBUG)
logger_screen_report.setLevel(logging.DEBUG)
logger_daily_at_ten.setLevel(logging.DEBUG)
logger_corporative_client_daily_report.setLevel(logging.DEBUG)

# Defining Logging Format
formatter_generate_daily_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_ibft_summary_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_manual_ibft_success = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_merchant_txn_list_after_job = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_every_sunday = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_generate_monthly_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_bank_topup_status_cs = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_screen_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_daily_at_ten = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_corporative_client_daily_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')

# Path for logging file handlers
file_handler_generate_daily_report = logging.FileHandler(db_log_path + 'generate_daily_report.log')
file_handler_ibft_summary_report = logging.FileHandler(db_log_path + 'ibft_summary_report.log')
file_handler_manual_ibft_success = logging.FileHandler(db_log_path + 'manual_ibft_success.log')
file_handler_merchant_txn_list_after_job = logging.FileHandler(db_log_path + 'merchant_txn_list_after_job.log')
file_handler_every_sunday = logging.FileHandler(db_log_path + 'every_sunday.log')
file_handler_generate_monthly_report = logging.FileHandler(db_log_path + 'generate_monthly_report.log')
file_handler_bank_topup_status_cs = logging.FileHandler(db_log_path + 'bank_topup_status_cs.log')
file_handler_screen_report = logging.FileHandler(db_log_path + 'screen_report.log')
file_handler_daily_at_ten = logging.FileHandler(db_log_path + 'daily_at_ten.log')
file_handler_corporative_client_daily_report = logging.FileHandler(db_log_path + 'corporative_client_daily_report.log')

# Setting Logging Level for File Handler
file_handler_generate_daily_report.setLevel(logging.DEBUG)
file_handler_ibft_summary_report.setLevel(logging.DEBUG)
file_handler_manual_ibft_success.setLevel(logging.DEBUG)
file_handler_merchant_txn_list_after_job.setLevel(logging.DEBUG)
file_handler_every_sunday.setLevel(logging.DEBUG)
file_handler_generate_monthly_report.setLevel(logging.DEBUG)
file_handler_bank_topup_status_cs.setLevel(logging.DEBUG)
file_handler_screen_report.setLevel(logging.DEBUG)
file_handler_daily_at_ten.setLevel(logging.DEBUG)
file_handler_corporative_client_daily_report.setLevel(logging.DEBUG)

# Setting Formatter to file handlers
file_handler_generate_daily_report.setFormatter(formatter_generate_daily_report)
file_handler_ibft_summary_report.setFormatter(formatter_ibft_summary_report)
file_handler_manual_ibft_success.setFormatter(formatter_manual_ibft_success)
file_handler_merchant_txn_list_after_job.setFormatter(formatter_merchant_txn_list_after_job)
file_handler_every_sunday.setFormatter(formatter_every_sunday)
file_handler_generate_monthly_report.setFormatter(formatter_generate_monthly_report)
file_handler_bank_topup_status_cs.setFormatter(formatter_bank_topup_status_cs)
file_handler_screen_report.setFormatter(formatter_screen_report)
file_handler_daily_at_ten.setFormatter(formatter_daily_at_ten)
file_handler_corporative_client_daily_report.setFormatter(formatter_corporative_client_daily_report)

# Adding file handler to logger
logger_generate_daily_report.addHandler(file_handler_generate_daily_report)
logger_ibft_summary_report.addHandler(file_handler_ibft_summary_report)
logger_manual_ibft_success.addHandler(file_handler_manual_ibft_success)
logger_merchant_txn_list_after_job.addHandler(file_handler_merchant_txn_list_after_job)
logger_every_sunday.addHandler(file_handler_every_sunday)
logger_generate_monthly_report.addHandler(file_handler_generate_monthly_report)
logger_bank_topup_status_cs.addHandler(file_handler_bank_topup_status_cs)
logger_screen_report.addHandler(file_handler_screen_report)
logger_daily_at_ten.addHandler(file_handler_daily_at_ten)
logger_corporative_client_daily_report.addHandler(file_handler_corporative_client_daily_report)

# Adding Stream Handler
stream_handler_generate_daily_report = logging.StreamHandler()
stream_handler_ibft_summary_report = logging.StreamHandler()
stream_handler_manual_ibft_success = logging.StreamHandler()
stream_handler_merchant_txn_list_after_job = logging.StreamHandler()
stream_handler_every_sunday = logging.StreamHandler()
stream_handler_generate_monthly_report = logging.StreamHandler()
stream_handler_bank_topup_status_cs = logging.StreamHandler()
stream_handler_screen_report = logging.StreamHandler()
stream_handler_daily_at_ten = logging.StreamHandler()
stream_handler_corporative_client_daily_report = logging.StreamHandler()

stream_handler_generate_daily_report.setFormatter(formatter_generate_daily_report)
stream_handler_ibft_summary_report.setFormatter(formatter_ibft_summary_report)
stream_handler_manual_ibft_success.setFormatter(formatter_manual_ibft_success)
stream_handler_merchant_txn_list_after_job.setFormatter(formatter_merchant_txn_list_after_job)
stream_handler_every_sunday.setFormatter(formatter_every_sunday)
stream_handler_generate_monthly_report.setFormatter(formatter_generate_monthly_report)
stream_handler_bank_topup_status_cs.setFormatter(formatter_bank_topup_status_cs)
stream_handler_screen_report.setFormatter(formatter_screen_report)
stream_handler_daily_at_ten.setFormatter(formatter_daily_at_ten)
stream_handler_corporative_client_daily_report.setFormatter(formatter_corporative_client_daily_report)

logger_generate_daily_report.addHandler(stream_handler_generate_daily_report)
logger_ibft_summary_report.addHandler(stream_handler_ibft_summary_report)
logger_manual_ibft_success.addHandler(stream_handler_manual_ibft_success)
logger_merchant_txn_list_after_job.addHandler(stream_handler_merchant_txn_list_after_job)
logger_every_sunday.addHandler(stream_handler_every_sunday)
logger_generate_monthly_report.addHandler(stream_handler_generate_monthly_report)
logger_bank_topup_status_cs.addHandler(stream_handler_bank_topup_status_cs)
logger_screen_report.addHandler(stream_handler_screen_report)
logger_daily_at_ten.addHandler(stream_handler_daily_at_ten)
logger_corporative_client_daily_report.addHandler(stream_handler_corporative_client_daily_report)


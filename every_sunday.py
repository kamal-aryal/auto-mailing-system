from datetime import datetime, timedelta

from shared.reports.html_default_config import *
from shared.reports.report import fetch_report, generate_csv_file, send_mail
from shared.reports.connection_params import *

from main_logger import logger_every_sunday


try:
    logger_every_sunday.info('Weekly Report generation started.')
    data_records = fetch_report('WEEKLY', 'EVERY_SUNDAY')
    logger_every_sunday.info('Report list to be generated are fetched.')
    for data in data_records:
        try:
            report_id = data[0]
            report_name = data[1]
            schedule = data[2]
            sql_query = data[3]
            is_html = data[4]
            db_connection_name = data[5]
            schedule_type = data[6]
            mail_to = str(data[7]).split(",")
            mail_cc = str(data[8]).split(",")
            mail_bcc = str(data[9]).split(",")
            mail_subject = data[10]
            mail_body = data[11]
            html_config_id = data[12]
            html_header = data[13]
            html_footer = data[14]
            html_signature = data[15]

            # Obtaining yesterday's date for report name concatenation
            yesterday_date = datetime.now() - timedelta(days=1)
            yesterday_date_str = '_' + yesterday_date.strftime('%Y-%m-%d_%H%M%S')
            mail_body_args = None

            csv_file_path = generate_csv_file(db_connection(db_connection_name),
                                              sql_query,
                                              report_name,
                                              yesterday_date_str)

            if is_html:
                # compose html file

                mail_body = html_composer(csv_file_path,
                                          html_config_id,
                                          html_header,
                                          html_footer,
                                          html_signature
                                          )

            if csv_file_path:
                # Send Mail
                send_mail_status = send_mail(mail_to,
                                             mail_cc,
                                             mail_bcc,
                                             mail_subject,
                                             mail_body,
                                             mail_body_args,
                                             csv_file_path,
                                             is_html,
                                             schedule_type)
                if send_mail_status == 1:
                    logger_every_sunday.info(f'Mail successfully sent for {report_name}.')
                else:
                    logger_every_sunday.warning(f'Error while sending mail for report: {report_name}')

        except Exception as e:
            logger_every_sunday.error(e, exc_info=True)

    logger_every_sunday.info('Weekly Report generation completed.')

except Exception as e:
    logger_every_sunday.error(e, exc_info=True)

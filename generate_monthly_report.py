import datetime
import nepali_datetime

from shared.reports.html_default_config import *
from shared.reports.report import fetch_report, generate_csv_file, send_mail
from shared.reports.connection_params import *
from shared.utilities.zip_file import zip_file

from main_logger import logger_generate_monthly_report

# Calculating From Date and To Date in AD
nepali_date_today = nepali_datetime.date.today()
today_day = nepali_date_today.day
today_month = nepali_date_today.month
today_year = nepali_date_today.year

previous_month_num = today_month-1
previous_month_year = today_year
if previous_month_num == 0:
    previous_month_year = today_year - 1
    previous_month_num = 12

previous_month = nepali_datetime.date(previous_month_year, previous_month_num, today_day)
previous_month_days = previous_month.days_in_month()
previous_month_name = previous_month.strftime("%B")

ad_from_date = nepali_date_today.to_datetime_date() - datetime.timedelta(previous_month_days)
ad_to_date = datetime.date.today()

if today_day == 1:
    try:
        logger_generate_monthly_report.info('Monthly Report generation started.')
        data_records = fetch_report('MONTHLY', 'DEFAULT_AMS')
        logger_generate_monthly_report.info('Report list to be generated are fetched.')
        for data in data_records:
            try:
                report_id = data[0]
                report_name = data[1]
                schedule = data[2]
                sql_query = data[3]
                is_html = data[4]
                db_connection_name = data[5]
                schedule_type = data[6]
                mail_to = str(data[7]).split(",")
                mail_cc = str(data[8]).split(",")
                mail_bcc = str(data[9]).split(",")
                mail_subject = data[10]
                mail_body = data[11]
                html_config_id = data[12]
                html_header = data[13]
                html_footer = data[14]
                html_signature = data[15]
                create_zip_file = data[16]
                encryption_value = data[17]

                temp_from_date = sql_query.replace('#FROM_DATE#', str(ad_from_date))
                temp_to_date = temp_from_date.replace('#TO_DATE#', str(ad_to_date))
                query_to_execute = temp_to_date

                # Obtaining yesterday's date for report name concatenation
                month_str = '_' + previous_month_name + '_' + str(previous_month_year)
                mail_body_args = None

                csv_file_path = generate_csv_file(db_connection(db_connection_name),
                                                  query_to_execute,
                                                  report_name,
                                                  month_str)

                if is_html:
                    # compose html file

                    mail_body = html_composer(csv_file_path,
                                              html_config_id,
                                              html_header,
                                              html_footer,
                                              html_signature
                                              )

                if csv_file_path:
                    destination_zip_file_path = csv_file_path.split('.')[0] + '.zip'

                    if create_zip_file:
                        append_date = previous_month
                        zip_file(report_name, csv_file_path, destination_zip_file_path, encryption_value, append_date)

                    # Send Mail
                    file_path = csv_file_path if not create_zip_file else destination_zip_file_path
                    send_mail_status = send_mail(mail_to,
                                                 mail_cc,
                                                 mail_bcc,
                                                 mail_subject,
                                                 mail_body,
                                                 mail_body_args,
                                                 file_path,
                                                 is_html,
                                                 schedule_type)
                    if send_mail_status == 1:
                        logger_generate_monthly_report.info(f'Mail successfully sent for {report_name}.')
                    else:
                        logger_generate_monthly_report.warning(f'Error while sending mail for report: {report_name}')
                else:
                    logger_generate_monthly_report.info(f'No records generated for report: {report_name}')

            except Exception as e:
                logger_generate_monthly_report.error(e, exc_info=True)

        logger_generate_monthly_report.info('Monthly Report generation completed.')
    except Exception as e:
        logger_generate_monthly_report.error(e, exc_info=True)
else:
    logger_generate_monthly_report.info('Not 1st day of the month.')

import pandas as pd


def html_default_header():
    default_header = """
    <html>
  <head>
  <style class="dataframe">
    table{{
      border-collapse: collapse;
      width: 100%;
      text-align: left;
      padding: 0px;
      font-size: 11px;
      color: black;

    }}
    th{{
      text-align: left;
      color: #FF2300;
    }}
    .regards{{
      margin-bottom: 0px;
      font-family: Calibri;
    }}
    .signature{{
      font-size: 12px;
      font-family: Calibri;
    }}
    a{{
      color: #51862f;
      text-decoration: none;
    }}
    .dear{{
      font-family: Calibri;
    }}
    .note{{
      font-family: Calibri;
    }}
  </style>
  </head>
  <body>
  <p> Dear All, \n\nPlease find the report attached.</p>
    """
    return default_header


def html_default_footer():
    default_footer="""
<p class="regards">
With Regards,<br>
DBA
</p>
    """
    return default_footer


def html_default_signature():
    default_signature = """
    <p class="signature">
    <strong>eSewa Fonepay Pvt. Ltd</strong><br>
    MNS Tower, Pulchowk, Lalitpur, Nepal<br>
    <a href='https://www.esewa.com.np'>www.esewa.com.np</a>   |  <a href="dba@esewa.com.np">dba@esewa.com.np</a>   |  GPO-8975, EPC-2626<br>
    <a href="https://www.facebook.com/esewa/">Facebook</a>  |  <a href="https://twitter.com/eSewaNepal">Twitter</a>  | <a href="http://chats.viber.com/eSewa">Viber</a>  |  <a href="https://www.youtube.com/channel/UC0Oa0oub50KuTUXPRs5V8XQ">Youtube</a><br>
    <a href="">+977 1 4442435</a>, <a href="">+977 1 4424743</a>  (Ext. 215)<br>
    <img src="https://cdn.esewa.com.np/signature/esewa/esewa.png">
    </p>
    </body>
    </html>
    """
    return default_signature


def html_composer(csv_file_path, html_config_id, html_header, html_footer, html_signature):
    df_read_csv = pd.read_csv(csv_file_path)
    df_html_table = df_read_csv.to_html(index=False)

    if html_config_id is None:
        header = html_default_header()
        footer = html_default_footer()
        signature = html_default_signature()
    else:
        header = html_header
        footer = html_footer
        signature = html_signature

    mail_body = header + df_html_table + footer + signature
    return mail_body

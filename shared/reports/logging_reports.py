import os
import logging


# Path for saving Log Files
db_helper_dir_path = os.path.abspath(os.path.dirname(__file__))
db_log_path = os.path.join(db_helper_dir_path, "./log_files/")

if not os.path.exists(os.path.dirname(db_log_path)):
    try:
        os.mkdir(db_log_path)
    except OSError as e:
        print(f'Directory creation failed: {db_log_path}')


# Initializing Logger for db_helper modules
logger_report = logging.getLogger('report')
logger_monthly_report = logging.getLogger('monthly_report')
# logger_generate_db_report = logging.getLogger('generate_db_report')

# Setting Logging Level
logger_report.setLevel(logging.DEBUG)
logger_monthly_report.setLevel(logging.DEBUG)
# logger_generate_db_report.setLevel(logging.DEBUG)

# Defining Logging Format
formatter_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_monthly_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
# formatter_generate_db_report = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')

# Path for logging file handlers
file_handler_report = logging.FileHandler(db_log_path + 'report.log')
file_handler_monthly_report = logging.FileHandler(db_log_path + 'monthly_report.log')
# file_handler_generate_db_report = logging.FileHandler(db_log_path + 'generate_db_report.log')

# Setting Logging Level for File Handler
file_handler_report.setLevel(logging.DEBUG)
file_handler_monthly_report.setLevel(logging.DEBUG)
# file_handler_generate_db_report.setLevel(logging.DEBUG)

# Setting Formatter to file handlers
file_handler_report.setFormatter(formatter_report)
file_handler_monthly_report.setFormatter(formatter_monthly_report)
# file_handler_generate_db_report.setFormatter(formatter_generate_db_report)

# Adding file handler to logger
logger_report.addHandler(file_handler_report)
logger_monthly_report.addHandler(file_handler_monthly_report)
# logger_generate_db_report.addHandler(file_handler_generate_db_report)

# Adding Stream Handler
stream_handler_report = logging.StreamHandler()
stream_handler_monthly_report = logging.StreamHandler()
# stream_handler_generate_db_report = logging.StreamHandler()

stream_handler_report.setFormatter(formatter_report)
stream_handler_monthly_report.setFormatter(formatter_monthly_report)
# stream_handler_generate_db_report.setFormatter(formatter_generate_db_report)

logger_report.addHandler(stream_handler_report)
logger_monthly_report.addHandler(stream_handler_monthly_report)
# logger_generate_db_report.addHandler(stream_handler_generate_db_report)

import os
import pandas as pd

from mailerpy import Mailer
from sqlalchemy import create_engine

from shared.reports.connection_params import *
from shared.reports.logging_reports import logger_report


csv_generation_path = 'D:\\eSewa Project\\esewa_services\\csv_files\\daily\\generated\\'
csv_archive_path = 'D:\\eSewa Project\\esewa_services\\csv_files\\daily\\archived\\'
if not os.path.exists(os.path.dirname(csv_generation_path)):
    try:
        os.makedirs(csv_generation_path)
    except OSError:
        logger_report.error(f"Creation of {csv_generation_path} directory failed.")

if not os.path.exists(os.path.dirname(csv_archive_path)):
    try:
        os.makedirs(csv_archive_path)
    except OSError:
        logger_report.error(f"Creation of {csv_archive_path} directory failed.")


def fetch_report(schedule, schedule_type):
    try:
        query = """
        SELECT
                  r.`id`,
                  r.`name`,
                  r.`schedule`,
                  r.`sql_query`,
                  r.`is_html`,
                  r.`db_connection`,
                  r.`schedule_type`,
                  mp.`mail_to`,
                  mp.`mail_cc`,
                  mp.`mail_bcc`,
                  mp.`mail_subject`,
                  mp.`mail_body`,
                  hc.`id` AS 'html_config_id',
                  hc.`header`,
                  hc.`footer`,
                  hc.`signature`,
                  r.`create_zip_file`,
                  r.`encryption_value`
                FROM
                  esewa_services.`reports` r
                JOIN esewa_services.`mail_properties` mp
                  ON mp.`report_id` = r.`id`
                LEFT JOIN esewa_services.`html_reports_config` hc
                  ON hc.`report_id` = r.`id`
                WHERE r.`report_status` ='ACTIVE'
                  AND r.`schedule` = '{0}'
                  AND r.`schedule_type` = '{1}'
                ORDER BY r.`priority_level`;
        """.format(schedule, schedule_type)

        engine = create_engine(str(CONNECT_DB_6), echo=False)
        connection = engine.connect()
        data_records = connection.execute(query)
        logger_report.info(f'Records are fetched for schedule: {schedule} and schedule_type:{schedule_type}')
        return data_records
    except Exception as e:
        logger_report.error(e, exc_info=True)


def generate_csv_file(db_connection_string, sql_query, report_name, append_datetime):

    report_engine = create_engine(db_connection_string, echo=False)
    report_connection = report_engine.connect()
    table_records = report_connection.execute(sql_query).fetchall()
    table_records_title = report_connection.execute(sql_query).keys()

    df_records = pd.DataFrame(table_records, columns=table_records_title)

    # Generating CSV files if the records are not empty
    if df_records.empty is False:
        # Exporting to csv file
        formatted_report_name = str(report_name).lower().replace(' ', '_')
        csv_file_path = csv_generation_path + formatted_report_name + append_datetime + '.csv'
        df_records.to_csv(csv_file_path, index=False)
        logger_report.debug(f'{report_name} csv file has been created')

        return csv_file_path

    return None


def send_mail(mail_to, mail_cc, mail_bcc, mail_subject, mail_body, mail_body_args, csv_file_path, is_html,
              schedule_type):
    try:

        mailer = Mailer(SMTP_HOST, SMTP_PORT, MAIL_FROM, MAIL_PASSWORD)
        mailer.send_mail(to_address=mail_to,
                         mail_cc=mail_cc,
                         mail_bcc=mail_bcc,
                         subject=mail_subject,
                         mail_body=mail_body,
                         mail_body_args=mail_body_args,
                         attachments=[csv_file_path] if not is_html else None,
                         file_as_html=is_html,
                         )
        return 1
    except Exception as e:
        logger_report.error(e, exc_info=True)



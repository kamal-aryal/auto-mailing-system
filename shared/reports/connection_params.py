import configparser

config = configparser.RawConfigParser()
config_path = 'D:\\eSewa Project\\esewa_services\\config_file.properties'
config.read(config_path)


# DB Connections parameters
CONNECT_DB_3 = config.get('DB_CONNECTION_PARAMETER', 'CONNECT_DB_3')
CONNECT_DB_6 = config.get('DB_CONNECTION_PARAMETER', 'CONNECT_DB_6')
CONNECT_DB_15 = config.get('DB_CONNECTION_PARAMETER', 'CONNECT_DB_15')
CONNECT_DB_9 = config.get('DB_CONNECTION_PARAMETER', 'CONNECT_DB_9')

# Mail Config
SMTP_HOST = config.get('MAIL_CONFIG', 'SMTP_HOST')
SMTP_PORT = config.get('MAIL_CONFIG', 'SMTP_PORT')
MAIL_FROM = config.get('MAIL_CONFIG', 'MAIL_FROM')
MAIL_PASSWORD = config.get('MAIL_CONFIG', 'MAIL_PASSWORD')


def db_connection(connect_db):
    all_db_connections = {
                            'CONNECT_DB_3': {
                                'connection_string': CONNECT_DB_3,
                            },
                            'CONNECT_DB_6': {
                                'connection_string': CONNECT_DB_6,
                            },
                            'CONNECT_DB_15': {
                                'connection_string': CONNECT_DB_15,
                            },
                            'CONNECT_DB_9': {
                                'connection_string': CONNECT_DB_9,
                            },
                        }

    db_connection_string = all_db_connections[connect_db]['connection_string']
    return db_connection_string

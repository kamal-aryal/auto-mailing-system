import imaplib
import email
import os
import configparser

from shared.utilities.logging_utilities import logger_read_mail

config = configparser.RawConfigParser()
config.read('D:\\eSewa Project\\esewa_services\\config_file.properties')

# Connection Parameters for IMAP
imap_host = config.get('IMAP_CONFIG', 'IMAP_HOST')
imap_port = int(config.get('IMAP_CONFIG', 'IMAP_PORT'))
imap_user = config.get('IMAP_CONFIG', 'IMAP_USER')
imap_pass = config.get('IMAP_CONFIG', 'IMAP_PASS')


recon_folder_list = ['D:\\eSewa Project\\esewa_services\\RECONCILITAION\\UPLOAD_PLACE\\',
                     'D:\\eSewa Project\\esewa_services\\RECONCILITAION\\SUCCESS_PLACE\\',
                     'D:\\eSewa Project\\esewa_services\\RECONCILITAION\\FAILED_PLACE\\',
                     'D:\\eSewa Project\\esewa_services\\RECONCILITAION\\PROCESS_PLACE\\'
                     ]

for each_folder in recon_folder_list:
    if not os.path.exists(os.path.dirname(each_folder)):
        try:
            os.makedirs(each_folder)
        except OSError:
            logger_read_mail.error(f"Creation of {each_folder} directory failed.")

mail_subject = 'CTP'
mailbox = 'Inbox'


# Reading from mail server to download all attachments
def read_email(mailbox_name, subject_name):
    try:
        imap_connect = imaplib.IMAP4_SSL(imap_host, imap_port)
        is_connected = imap_connect.login(imap_user, imap_pass)
        if is_connected[0] == 'OK':
            logger_read_mail.info(f'Successfully connected to IMAP host')
            imap_connect.select(mailbox_name)
            tmp, data = imap_connect.search(None, '(UNSEEN SUBJECT "%s")' % subject_name)

            if data[0].split():
                # To read the email
                for num in data[0].split():
                    tmp, data = imap_connect.fetch(num, '(RFC822)')
                    raw_email = data[0][1]
                    raw_email_string = raw_email.decode('utf-8')
                    email_msg = email.message_from_string(raw_email_string)
                    break

                # To download the attachments from the mail
                for attachment in email_msg.walk():
                    if attachment.get_content_maintype() == 'multipart/alternative':
                        continue
                    if attachment.get('Content-Disposition') is None:
                        continue
                    file_name = attachment.get_filename()
                    file_extension = file_name.split('.')[1]

                    if bool(file_name) and (file_extension == 'csv' or file_extension == 'xlsx'):
                        file_path = os.path.join('D:\\eSewa Project\\esewa_services\\RECONCILITAION\\UPLOAD_PLACE\\', file_name)
                        if not os.path.isfile(file_path):
                            with open(file_path, 'wb') as fp:
                                fp.write(attachment.get_payload(decode=True))
                            logger_read_mail.info(f"Downloaded filename: {file_name}")
                    else:
                        logger_read_mail.info('No files with csv or xlsx extension found in the mail.')
            else:
                logger_read_mail.info('No new mail to read.')
    except Exception as e:
        logger_read_mail.error(e, exc_info=True)
    finally:
        imap_connect.close()
        logger_read_mail.info('Connection to the mail server closed.')


read_email(mailbox, mail_subject)

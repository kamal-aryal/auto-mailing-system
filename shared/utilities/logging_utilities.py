import os
import logging


# Path for saving Log Files
db_helper_dir_path = os.path.abspath(os.path.dirname(__file__))
db_log_path = os.path.join(db_helper_dir_path, "./log_files/")

if not os.path.exists(os.path.dirname(db_log_path)):
    try:
        os.mkdir(db_log_path)
    except OSError as e:
        print(f'Directory creation failed: {db_log_path}')


# Initializing Logger for db_helper modules
logger_read_mail = logging.getLogger('read_mail')
logger_zip_file = logging.getLogger('zip_file')

# Setting Logging Level
logger_read_mail.setLevel(logging.DEBUG)
logger_zip_file.setLevel(logging.DEBUG)

# Defining Logging Format
formatter_read_mail = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')
formatter_zip_file = logging.Formatter('%(levelname)s:%(name)s:%(asctime)s:%(message)s')

# Path for logging file handlers
file_handler_read_mail = logging.FileHandler(db_log_path + 'read_mail.log')
file_handler_zip_file = logging.FileHandler(db_log_path + 'zip_file.log')

# Setting Logging Level for File Handler
file_handler_read_mail.setLevel(logging.DEBUG)
file_handler_zip_file.setLevel(logging.DEBUG)

# Setting Formatter to file handlers
file_handler_read_mail.setFormatter(formatter_read_mail)
file_handler_zip_file.setFormatter(formatter_zip_file)

# Adding file handler to logger
logger_read_mail.addHandler(file_handler_read_mail)
logger_zip_file.addHandler(file_handler_zip_file)

# Adding Stream Handler
stream_handler_read_mail = logging.StreamHandler()
stream_handler_zip_file = logging.StreamHandler()

stream_handler_read_mail.setFormatter(formatter_read_mail)
stream_handler_zip_file.setFormatter(formatter_zip_file)

logger_read_mail.addHandler(stream_handler_read_mail)
logger_zip_file.addHandler(stream_handler_zip_file)

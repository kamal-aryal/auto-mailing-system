import base64
# import pyminizip

from shared.utilities.logging_utilities import logger_zip_file


def zip_file(file_name, source_file_path, destination_file_path, password, append_date):
    try:
        prefix_path = None
        compression_lvl = 9

        if password is not None:
            decoded_password = base64.b64decode(password).decode('utf-8')
            zip_password = decoded_password + append_date
        else:
            zip_password = None

        # pyminizip.compress(source_file_path, prefix_path, destination_file_path, zip_password, compression_lvl)
        logger_zip_file.info(f"Zip file created for file: {file_name}")
    except Exception as e:
        logger_zip_file.error(e, exc_info=True)